<?php

namespace App\Services;

use Exception;
use App\Models\Catable;
use Illuminate\Log\Logger;
use Illuminate\Database\DatabaseManager;
use App\Repositories\Contracts\CatableRepository;
use App\Services\Contracts\CatableService as CatableServiceInterface;

/**
 * @method bool destroy
 */
class CatableServiceEntity  extends BaseService implements CatableServiceInterface
{

    /**
     * @var DatabaseManager $databaseManager
     */
    protected $databaseManager;

    /**
     * @var CatableRepository $repository
     */
    protected $repository;

    /**
     * Language $language
     */
    protected $language;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * Catable constructor.
     *
     * @param DatabaseManager $databaseManager
     * @param CatableRepository $repository
     * @param Logger $logger
     */
    public function __construct(
        DatabaseManager $databaseManager,
        CatableRepository $repository,
        Logger $logger
    ) {

        $this->databaseManager     = $databaseManager;
        $this->repository     = $repository;
        $this->logger     = $logger;
    }

    /**
     * Create Catable
     *
     * @param array $data
     * @return Catable
     * @throws \Exception
     */
    public function store(array $data)
    {
        $this->beginTransaction();
        try {
            $model = $this->repository->newInstance();

            $model->fill($data);

            if (!$model->save()) {
                throw new Exception('Catable was not saved to the database.');
            }

            $this->logger->info('Catable successfully saved.', ['model_id' => $model->id]);
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while storing an ', [
                'data' => $data,
            ]);
        }

        $this->commit();
        return $model;
    }

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @param  array  $data
     *
     * @return Catable
     *
     * @throws
     */
    public function update($id, array $data)
    {
        $this->beginTransaction();

        try {
            $model = $this->repository->update($data, $id);

            if (!$model) {
                throw new Exception('An error occurred while updating a Catable');
            }

            $this->logger->info('Catable  was successfully updated.');
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while updating an articles.', [
                'id'   => $id,
                'data' => $data,
            ]);
        }
        $this->commit();
        return $model;
    }
    /**
     * Delete block in the storage.
     *
     * @param  int  $id
     *
     * @return bool
     *
     * @throws
     */
    public function delete($id)
    {

        $this->beginTransaction();

        try {
            $bufferCategory = [];
            $model = $this->repository->find($id);

            if (!$model->delete()) {
                throw new Exception(
                    'Catable and  translations was not deleted from database.'
                );
            }
            $this->logger->info('Catable  was successfully deleted from database.');
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while deleting an Catable.', [
                'id'   => $id,
            ]);
        }
        $this->commit();
        return true;
    }

}
