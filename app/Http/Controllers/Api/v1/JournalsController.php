<?php

namespace App\Http\Controllers\Api\v1;
use App\Http\Resources\DTOCollection;
use App\Transformers\JournalTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Routing\Controller;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\JournalCreateRequest;
use App\Http\Requests\JournalUpdateRequest;
use App\Repositories\Contracts\JournalRepository;
use App\Services\Contracts\JournalService;
use App\Validators\JournalValidator;

/**
 * Class JournalsController.
 *
 * @package namespace App\Http\Controllers\Api\v1;
 */
class JournalsController extends Controller
{
    /**
     * @var JournalRepository
     */
    protected $repository;

    /**
     * @var JournalValidator
     */
    protected $validator;

    /**
     * @var JournalService
     */
    protected $service;

    /**
     * JournalsController constructor.
     *
     * @param JournalRepository $repository
     * @param JournalValidator $validator
     * @param JournalService $service
     */
    public function __construct(
        JournalRepository $repository,
        JournalValidator $validator,
        JournalService $service
    ) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service    = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit', 10);
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $journals = new DTOCollection($this->repository->paginate($limit), '$CLASS');
        return response()->json($journals);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  JournalCreateRequest $request
     *
     * @return JsonResponse
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(JournalCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $journal = $this->service->store($request->all());

            $response = [
                'message' => 'Journal created.',
                'data'    => new JournalTransformer($journal),
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function show($id)
    {
        $journal = new JournalTransformer($this->repository->findOrFail($id));

        return response()->json([
           'data' => $journal,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function edit($id)
    {
        $journal = new JournalTransformer($this->repository->findOrFail($id));

        return response()->json([
                   'data' => $journal,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  JournalUpdateRequest $request
     * @param  string            $id
     *
     * @return JsonResponse
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(JournalUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $journal = $this->service->update($id, $request->all());

            $response = [
                'message' => 'Journal updated.',
                'data'    => new JournalTransformer($journal),
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {

            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $deleted = $this->service->delete($id);

        return response()->json([
            'message' => 'Journal deleted.',
            'deleted' => $deleted,
        ]);
    }
}
