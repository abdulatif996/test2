<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Catable;

/**
 * Class CatableTransformer.
 *
 * @package namespace App\Transformers;
 */
class CatableTransformer extends TransformerAbstract
{
    /**
     * Transform the Catable entity.
     *
     * @param \App\Models\Catable $model
     *
     * @return array
     */
    public function transform(Catable $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
