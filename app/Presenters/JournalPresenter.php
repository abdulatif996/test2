<?php

namespace App\Presenters;

use App\Transformers\JournalTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class JournalPresenter.
 *
 * @package namespace App\Presenters;
 */
class JournalPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new JournalTransformer();
    }
}
