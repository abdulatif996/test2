<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Post1;

/**
 * Class Post1Transformer.
 *
 * @package namespace App\Transformers;
 */
class Post1Transformer extends TransformerAbstract
{
    /**
     * Transform the Post1 entity.
     *
     * @param \App\Entities\Post1 $model
     *
     * @return array
     */
    public function transform(Post1 $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
