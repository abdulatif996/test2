<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TransactionRepository.
 *
 * @package namespace App\Repositories\Contracts;
 */
interface TransactionRepository extends RepositoryInterface
{
    //
}
