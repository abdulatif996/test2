<?php

namespace App\Presenters;

use App\Transformers\CatableTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CatablePresenter.
 *
 * @package namespace App\Presenters;
 */
class CatablePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CatableTransformer();
    }
}
