<?php

namespace App\Services;

use Exception;
use App\Models\Cat;
use Illuminate\Log\Logger;
use Illuminate\Database\DatabaseManager;
use App\Repositories\Contracts\CatRepository;
use App\Services\Contracts\CatService as CatServiceInterface;

/**
 * @method bool destroy
 */
class CatServiceEntity  extends BaseService implements CatServiceInterface
{

    /**
     * @var DatabaseManager $databaseManager
     */
    protected $databaseManager;

    /**
     * @var CatRepository $repository
     */
    protected $repository;

    /**
     * Language $language
     */
    protected $language;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * Cat constructor.
     *
     * @param DatabaseManager $databaseManager
     * @param CatRepository $repository
     * @param Logger $logger
     */
    public function __construct(
        DatabaseManager $databaseManager,
        CatRepository $repository,
        Logger $logger
    ) {

        $this->databaseManager     = $databaseManager;
        $this->repository     = $repository;
        $this->logger     = $logger;
    }

    /**
     * Create Cat
     *
     * @param array $data
     * @return Cat
     * @throws \Exception
     */
    public function store(array $data)
    {
        $this->beginTransaction();
        try {
            $model = $this->repository->newInstance();

            $model->fill($data);

            if (!$model->save()) {
                throw new Exception('Cat was not saved to the database.');
            }

            $this->logger->info('Cat successfully saved.', ['model_id' => $model->id]);
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while storing an ', [
                'data' => $data,
            ]);
        }

        $this->commit();
        return $model;
    }

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @param  array  $data
     *
     * @return Cat
     *
     * @throws
     */
    public function update($id, array $data)
    {
        $this->beginTransaction();

        try {
            $model = $this->repository->update($data, $id);

            if (!$model) {
                throw new Exception('An error occurred while updating a Cat');
            }

            $this->logger->info('Cat  was successfully updated.');
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while updating an articles.', [
                'id'   => $id,
                'data' => $data,
            ]);
        }
        $this->commit();
        return $model;
    }
    /**
     * Delete block in the storage.
     *
     * @param  int  $id
     *
     * @return bool
     *
     * @throws
     */
    public function delete($id)
    {

        $this->beginTransaction();

        try {
            $bufferCategory = [];
            $model = $this->repository->find($id);

            if (!$model->delete()) {
                throw new Exception(
                    'Cat and  translations was not deleted from database.'
                );
            }
            $this->logger->info('Cat  was successfully deleted from database.');
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while deleting an Cat.', [
                'id'   => $id,
            ]);
        }
        $this->commit();
        return true;
    }

}
