<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface Post1Repository.
 *
 * @package namespace App\Repositories;
 */
interface Post1Repository extends RepositoryInterface
{
    //
}
