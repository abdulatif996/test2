<?php

namespace App\Services\Contracts;

use App\Models\Cat;

/**
 * Interface CatService.
 *
 * @package namespace App\Services\Contracts;
 */
interface CatService extends BaseService
{
    /**
     * Store a newly created resource in storage
     *
     * @param array $data
     * @return Cat
     */
    public function store(array $data);

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @param  array  $data
     * @return Cat
     */
    public function update($id, array $data);

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function delete($id);
}
