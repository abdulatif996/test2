<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CatRepository.
 *
 * @package namespace App\Repositories\Contracts;
 */
interface CatRepository extends RepositoryInterface
{
    //
}
