<?php

namespace App\Services;

use Exception;
use App\Models\Journal;
use Illuminate\Log\Logger;
use Illuminate\Database\DatabaseManager;
use App\Repositories\Contracts\JournalRepository;
use App\Services\Contracts\JournalService as JournalServiceInterface;

/**
 * @method bool destroy
 */
class JournalServiceEntity  extends BaseService implements JournalServiceInterface
{

    /**
     * @var DatabaseManager $databaseManager
     */
    protected $databaseManager;

    /**
     * @var JournalRepository $repository
     */
    protected $repository;

    /**
     * Language $language
     */
    protected $language;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * Journal constructor.
     *
     * @param DatabaseManager $databaseManager
     * @param JournalRepository $repository
     * @param Logger $logger
     */
    public function __construct(
        DatabaseManager $databaseManager,
        JournalRepository $repository,
        Logger $logger
    ) {

        $this->databaseManager     = $databaseManager;
        $this->repository     = $repository;
        $this->logger     = $logger;
    }

    /**
     * Create Journal
     *
     * @param array $data
     * @return Journal
     * @throws \Exception
     */
    public function store(array $data)
    {
        $this->beginTransaction();
        try {
            $model = $this->repository->newInstance();

            $model->fill($data);

            if (!$model->save()) {
                throw new Exception('Journal was not saved to the database.');
            }

            $this->logger->info('Journal successfully saved.', ['model_id' => $model->id]);
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while storing an ', [
                'data' => $data,
            ]);
        }

        $this->commit();
        return $model;
    }

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @param  array  $data
     *
     * @return Journal
     *
     * @throws
     */
    public function update($id, array $data)
    {
        $this->beginTransaction();

        try {
            $model = $this->repository->update($data, $id);

            if (!$model) {
                throw new Exception('An error occurred while updating a Journal');
            }

            $this->logger->info('Journal  was successfully updated.');
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while updating an articles.', [
                'id'   => $id,
                'data' => $data,
            ]);
        }
        $this->commit();
        return $model;
    }
    /**
     * Delete block in the storage.
     *
     * @param  int  $id
     *
     * @return bool
     *
     * @throws
     */
    public function delete($id)
    {

        $this->beginTransaction();

        try {
            $bufferCategory = [];
            $model = $this->repository->find($id);

            if (!$model->delete()) {
                throw new Exception(
                    'Journal and  translations was not deleted from database.'
                );
            }
            $this->logger->info('Journal  was successfully deleted from database.');
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while deleting an Journal.', [
                'id'   => $id,
            ]);
        }
        $this->commit();
        return true;
    }

}
