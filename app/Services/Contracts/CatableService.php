<?php

namespace App\Services\Contracts;

use App\Models\Catable;

/**
 * Interface CatableService.
 *
 * @package namespace App\Services\Contracts;
 */
interface CatableService extends BaseService
{
    /**
     * Store a newly created resource in storage
     *
     * @param array $data
     * @return Catable
     */
    public function store(array $data);

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @param  array  $data
     * @return Catable
     */
    public function update($id, array $data);

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function delete($id);
}
