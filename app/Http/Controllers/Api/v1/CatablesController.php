<?php

namespace App\Http\Controllers\Api\v1;
use App\Http\Resources\DTOCollection;
use App\Transformers\CatableTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Routing\Controller;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\CatableCreateRequest;
use App\Http\Requests\CatableUpdateRequest;
use App\Repositories\Contracts\CatableRepository;
use App\Services\Contracts\CatableService;
use App\Validators\CatableValidator;

/**
 * Class CatablesController.
 *
 * @package namespace App\Http\Controllers\Api\v1;
 */
class CatablesController extends Controller
{
    /**
     * @var CatableRepository
     */
    protected $repository;

    /**
     * @var CatableValidator
     */
    protected $validator;

    /**
     * @var CatableService
     */
    protected $service;

    /**
     * CatablesController constructor.
     *
     * @param CatableRepository $repository
     * @param CatableValidator $validator
     * @param CatableService $service
     */
    public function __construct(
        CatableRepository $repository,
        CatableValidator $validator,
        CatableService $service
    ) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service    = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit', 10);
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $catables = new DTOCollection($this->repository->paginate($limit), '$CLASS');
        return response()->json($catables);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CatableCreateRequest $request
     *
     * @return JsonResponse
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CatableCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $catable = $this->service->store($request->all());

            $response = [
                'message' => 'Catable created.',
                'data'    => new CatableTransformer($catable),
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function show($id)
    {
        $catable = new CatableTransformer($this->repository->findOrFail($id));

        return response()->json([
           'data' => $catable,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function edit($id)
    {
        $catable = new CatableTransformer($this->repository->findOrFail($id));

        return response()->json([
                   'data' => $catable,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CatableUpdateRequest $request
     * @param  string            $id
     *
     * @return JsonResponse
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CatableUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $catable = $this->service->update($id, $request->all());

            $response = [
                'message' => 'Catable updated.',
                'data'    => new CatableTransformer($catable),
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {

            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $deleted = $this->service->delete($id);

        return response()->json([
            'message' => 'Catable deleted.',
            'deleted' => $deleted,
        ]);
    }
}
