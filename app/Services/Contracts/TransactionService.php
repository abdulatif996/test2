<?php

namespace App\Services\Contracts;

use App\Models\Transaction;

/**
 * Interface TransactionService.
 *
 * @package namespace App\Services\Contracts;
 */
interface TransactionService extends BaseService
{
    /**
     * Store a newly created resource in storage
     *
     * @param array $data
     * @return Transaction
     */
    public function store(array $data);

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @param  array  $data
     * @return Transaction
     */
    public function update($id, array $data);

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @return bool
     */
    public function delete($id);
}
