<?php

namespace App\Presenters;

use App\Transformers\Post1Transformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class Post1Presenter.
 *
 * @package namespace App\Presenters;
 */
class Post1Presenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new Post1Transformer();
    }
}
