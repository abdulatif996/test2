<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\Post1Repository::class, \App\Repositories\Post1RepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TagRepository::class, \App\Repositories\TagRepositoryEloquent::class);
        $this->app->bind(\App\Services\Contracts\TransactionService::class, \App\Services\TransactionServiceEntity::class);
        $this->app->bind(\App\Services\Contracts\OrderService::class, \App\Services\OrderServiceEntity::class);
        $this->app->bind(\App\Services\Contracts\CatService::class, \App\Services\CatServiceEntity::class);
        $this->app->bind(\App\Services\Contracts\CatableService::class, \App\Services\CatableServiceEntity::class);
        $this->app->bind(\App\Services\Contracts\JournalService::class, \App\Services\JournalServiceEntity::class);
        //:end-bindings:
    }
}
