<?php

namespace App\Http\Controllers\Api\v1;
use App\Http\Resources\DTOCollection;
use App\Transformers\TransactionTransformer as TransactionDto;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Routing\Controller;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\TransactionCreateRequest;
use App\Http\Requests\TransactionUpdateRequest;
use App\Repositories\Contracts\TransactionRepository;
use App\Services\Contracts\TransactionService;
use App\Validators\TransactionValidator;

/**
 * Class TransactionsController.
 *
 * @package namespace App\Http\Controllers\Api\v1;
 */
class TransactionsController extends Controller
{
    /**
     * @var TransactionRepository
     */
    protected $repository;

    /**
     * @var TransactionValidator
     */
    protected $validator;

    /**
     * @var TransactionService
     */
    protected $service;

    /**
     * TransactionsController constructor.
     *
     * @param TransactionRepository $repository
     * @param TransactionValidator $validator
     * @param TransactionService $service
     */
    public function __construct(
        TransactionRepository $repository,
        TransactionValidator $validator,
        TransactionService $service
    ) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service    = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit', 10);
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $transactions = new DTOCollection($this->repository->paginate($limit), '$CLASS');
        return response()->json($transactions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TransactionCreateRequest $request
     *
     * @return JsonResponse
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(TransactionCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $transaction = $this->service->store($request->all());

            $response = [
                'message' => 'Transaction created.',
                'data'    => new TransactionDto($transaction),
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function show($id)
    {
        $transaction = new TransactionDto($this->repository->findOrFail($id));

        return response()->json([
           'data' => $transaction,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function edit($id)
    {
        $transaction = new TransactionDto($this->repository->findOrFail($id));

        return response()->json([
                   'data' => $transaction,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TransactionUpdateRequest $request
     * @param  string            $id
     *
     * @return JsonResponse
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(TransactionUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $transaction = $this->service->update($id, $request->all());

            $response = [
                'message' => 'Transaction updated.',
                'data'    => new TransactionDto($transaction),
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {

            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $deleted = $this->service->delete($id);

        return response()->json([
            'message' => 'Transaction deleted.',
            'deleted' => $deleted,
        ]);
    }
}
