<?php

namespace App\Services;

use Exception;
use App\Models\Transaction;
use Illuminate\Log\Logger;
use Illuminate\Database\DatabaseManager;
use App\Repositories\Contracts\TransactionRepository;
use App\Services\Contracts\TransactionService as TransactionServiceInterface;

/**
 * @method bool destroy
 */
class TransactionServiceEntity  extends BaseService implements TransactionServiceInterface
{

    /**
     * @var DatabaseManager $databaseManager
     */
    protected $databaseManager;

    /**
     * @var TransactionRepository $repository
     */
    protected $repository;

    /**
     * Language $language
     */
    protected $language;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * Transaction constructor.
     *
     * @param DatabaseManager $databaseManager
     * @param TransactionRepository $repository
     * @param Logger $logger
     */
    public function __construct(
        DatabaseManager $databaseManager,
        TransactionRepository $repository,
        Logger $logger
    ) {

        $this->databaseManager     = $databaseManager;
        $this->repository     = $repository;
        $this->logger     = $logger;
    }

    /**
     * Create Transaction
     *
     * @param array $data
     * @return Transaction
     * @throws \Exception
     */
    public function store(array $data)
    {
        $this->beginTransaction();
        try {
            $model = $this->repository->newInstance();

            $model->fill($data);

            if (!$model->save()) {
                throw new Exception('Transaction was not saved to the database.');
            }

            $this->logger->info('Transaction successfully saved.', ['model_id' => $model->id]);
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while storing an ', [
                'data' => $data,
            ]);
        }

        $this->commit();
        return $model;
    }

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @param  array  $data
     *
     * @return Transaction
     *
     * @throws
     */
    public function update($id, array $data)
    {
        $this->beginTransaction();

        try {
            $model = $this->repository->update($request->all(), $id);

            if (!$model) {
                throw new Exception('An error occurred while updating a Transaction');
            }

            $this->logger->info('Transaction  was successfully updated.');
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while updating an articles.', [
                'id'   => $id,
                'data' => $data,
            ]);
        }
        $this->commit();
        return $model;
    }
    /**
     * Delete block in the storage.
     *
     * @param  int  $id
     *
     * @return bool
     *
     * @throws
     */
    public function delete($id)
    {

        $this->beginTransaction();

        try {
            $bufferCategory = [];
            $model = $this->repository->find($id);

            if (!$model->delete()) {
                throw new Exception(
                    'Transaction and  translations was not deleted from database.'
                );
            }
            $this->logger->info('Transaction  was successfully deleted from database.');
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while deleting an Transaction.', [
                'id'   => $id,
            ]);
        }
        $this->commit();
        return true;
    }

}
