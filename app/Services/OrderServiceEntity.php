<?php

namespace App\Services;

use Exception;
use App\Models\Order;
use Illuminate\Log\Logger;
use Illuminate\Database\DatabaseManager;
use App\Repositories\Contracts\OrderRepository;
use App\Services\Contracts\OrderService as OrderServiceInterface;

/**
 * @method bool destroy
 */
class OrderServiceEntity  extends BaseService implements OrderServiceInterface
{

    /**
     * @var DatabaseManager $databaseManager
     */
    protected $databaseManager;

    /**
     * @var OrderRepository $repository
     */
    protected $repository;

    /**
     * Language $language
     */
    protected $language;

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * Order constructor.
     *
     * @param DatabaseManager $databaseManager
     * @param OrderRepository $repository
     * @param Logger $logger
     */
    public function __construct(
        DatabaseManager $databaseManager,
        OrderRepository $repository,
        Logger $logger
    ) {

        $this->databaseManager     = $databaseManager;
        $this->repository     = $repository;
        $this->logger     = $logger;
    }

    /**
     * Create Order
     *
     * @param array $data
     * @return Order
     * @throws \Exception
     */
    public function store(array $data)
    {
        $this->beginTransaction();
        try {
            $model = $this->repository->newInstance();

            $model->fill($data);

            if (!$model->save()) {
                throw new Exception('Order was not saved to the database.');
            }

            $this->logger->info('Order successfully saved.', ['model_id' => $model->id]);
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while storing an ', [
                'data' => $data,
            ]);
        }

        $this->commit();
        return $model;
    }

    /**
     * Update block in the storage.
     *
     * @param  int  $id
     * @param  array  $data
     *
     * @return Order
     *
     * @throws
     */
    public function update($id, array $data)
    {
        $this->beginTransaction();

        try {
            $model = $this->repository->update($data, $id);

            if (!$model) {
                throw new Exception('An error occurred while updating a Order');
            }

            $this->logger->info('Order  was successfully updated.');
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while updating an articles.', [
                'id'   => $id,
                'data' => $data,
            ]);
        }
        $this->commit();
        return $model;
    }
    /**
     * Delete block in the storage.
     *
     * @param  int  $id
     *
     * @return bool
     *
     * @throws
     */
    public function delete($id)
    {

        $this->beginTransaction();

        try {
            $bufferCategory = [];
            $model = $this->repository->find($id);

            if (!$model->delete()) {
                throw new Exception(
                    'Order and  translations was not deleted from database.'
                );
            }
            $this->logger->info('Order  was successfully deleted from database.');
        } catch (Exception $e) {
            $this->rollback($e, 'An error occurred while deleting an Order.', [
                'id'   => $id,
            ]);
        }
        $this->commit();
        return true;
    }

}
