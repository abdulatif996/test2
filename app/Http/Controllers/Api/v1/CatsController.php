<?php

namespace App\Http\Controllers\Api\v1;
use App\Http\Resources\DTOCollection;
use App\Transformers\CatTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Routing\Controller;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\CatCreateRequest;
use App\Http\Requests\CatUpdateRequest;
use App\Repositories\Contracts\CatRepository;
use App\Services\Contracts\CatService;
use App\Validators\CatValidator;

/**
 * Class CatsController.
 *
 * @package namespace App\Http\Controllers\Api\v1;
 */
class CatsController extends Controller
{
    /**
     * @var CatRepository
     */
    protected $repository;

    /**
     * @var CatValidator
     */
    protected $validator;

    /**
     * @var CatService
     */
    protected $service;

    /**
     * CatsController constructor.
     *
     * @param CatRepository $repository
     * @param CatValidator $validator
     * @param CatService $service
     */
    public function __construct(
        CatRepository $repository,
        CatValidator $validator,
        CatService $service
    ) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service    = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $limit = $request->get('limit', 10);
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $cats = new DTOCollection($this->repository->paginate($limit), '$CLASS');
        return response()->json($cats);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CatCreateRequest $request
     *
     * @return JsonResponse
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CatCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $cat = $this->service->store($request->all());

            $response = [
                'message' => 'Cat created.',
                'data'    => new CatTransformer($cat),
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function show($id)
    {
        $cat = new CatTransformer($this->repository->findOrFail($id));

        return response()->json([
           'data' => $cat,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function edit($id)
    {
        $cat = new CatTransformer($this->repository->findOrFail($id));

        return response()->json([
                   'data' => $cat,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CatUpdateRequest $request
     * @param  string            $id
     *
     * @return JsonResponse
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CatUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $cat = $this->service->update($id, $request->all());

            $response = [
                'message' => 'Cat updated.',
                'data'    => new CatTransformer($cat),
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {

            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $deleted = $this->service->delete($id);

        return response()->json([
            'message' => 'Cat deleted.',
            'deleted' => $deleted,
        ]);
    }
}
