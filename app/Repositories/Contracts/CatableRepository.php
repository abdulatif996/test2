<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CatableRepository.
 *
 * @package namespace App\Repositories\Contracts;
 */
interface CatableRepository extends RepositoryInterface
{
    //
}
