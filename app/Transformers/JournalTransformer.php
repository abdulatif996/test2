<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Journal;

/**
 * Class JournalTransformer.
 *
 * @package namespace App\Transformers;
 */
class JournalTransformer extends TransformerAbstract
{
    /**
     * Transform the Journal entity.
     *
     * @param \App\Models\Journal $model
     *
     * @return array
     */
    public function transform(Journal $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
