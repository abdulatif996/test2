<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Post1Repository;
use App\Entities\Post1;
use App\Validators\Post1Validator;

/**
 * Class Post1RepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class Post1RepositoryEloquent extends BaseRepository implements Post1Repository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Post1::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return Post1Validator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
